# Western Medieval Knights

type = cavalry
unit_type = tech_gerudian

maneuver = 2
offensive_morale = 3
defensive_morale = 0
offensive_fire = 1
defensive_fire = 0
offensive_shock = 4
defensive_shock = 1

trigger = {
	OR = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}